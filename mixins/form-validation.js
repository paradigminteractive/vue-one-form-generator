/* eslint-disable no-underscore-dangle, no-unused-vars */

import {
  merge, pick, isEqual, sortBy, omit,
} from 'lodash';
import { getValidator } from '../validators';
import { mapObjectValues, generateObject, valueExists } from '../utilities';

module.exports = ({
  modelProperty = 'model',
  activeModelProperty = 'model',
  customValidation = 'customValidation',
} = {}) => ({
  props: {
    locale: {
      type: String,
    },
    /**
     * { field: { rule: { parameters }}}
     */
    externalValidation: {
      type: Object,
      default: () => ({}),
    },
  },
  data() {
    return {
      validation: {},
      v_activeKeys: [],
      v_fieldsStates: {},
    };
  },
  watch: {
    [activeModelProperty]: {
      handler() {
        this.v_updateModelStructure();
      },
      deep: true,
    },
  },
  ready() {
    this.v_updateFieldsStates();
  },
  computed: {
    isValid() {
      return !this.v_activeKeys.find(this.v_fails.bind(this));
    },
    v_config() {
      const countryCode = this.locale || this.themeConfig.countryCode;
      return merge(
        {},
        this.v_customValidation,
        this.externalValidation,
      )[this.locale.toLowerCase()];
    },
    v_model() {
      return this[modelProperty];
    },
    v_activeModel() {
      return this[activeModelProperty];
    },
    v_customValidation() {
      return this[customValidation] || {};
    },
  },
  methods: {
    resetValidation() {
      this.validation = {};
      this.v_activeKeys = [];
      this.v_fieldsStates = {};
      this.v_updateModelStructure();
    },
    validateModel(model) {
      this.v_updateFieldState(model);
      if (this.v_model[model] === undefined) return this.validation[model];
      const results = this.v_validate(model, this.v_getRules(model));
      Vue.set(this.validation, model, this.v_generateValidation(model, results));
      return this.validation[model];
    },
    validate(models = this.v_activeKeys) {
      models.forEach((model) => {
        this.v_updateFieldState(model, { dirty: true });
        this.validateModel(model);
      });
    },
    v_updateModelStructure() {
      const newModelKeys = Object.keys(this.v_activeModel);
      if (isEqual(sortBy(newModelKeys), sortBy(this.v_activeKeys))) return;
      this.v_activeKeys = newModelKeys;
      this.v_updateFieldsStates();
    },
    v_getRules(model) {
      if (!this.v_config) return {}; // needed for ie11 (?)
      return this.v_config[model] || {};
    },
    v_generateValidation(model, validationResult) {
      return {
        model,
        ...validationResult,
        handlers: this.v_getHandlers(model),
      };
    },
    v_getDefaultRuleState() {
      return {
        active: true,
        pass: false,
      };
    },
    v_getDefaultFieldState() {
      return {
        active: true,
        touched: false,
        dirty: false,
        optional: false,
        valid: false,
        invalid: false,
      };
    },
    v_getHandlers(model) {
      return {
        touched: () => this.v_handleTouch(model),
        blurred: () => this.v_handleBlur(model),
      };
    },
    v_updateFieldsStates() {
      this.v_activeKeys.forEach(this.v_updateFieldState.bind(this));
    },
    v_updateFieldState(model, newState = {}) {
      const currentState = Object.assign({}, this.v_fieldsStates[model], newState);

      currentState.dirty = currentState.dirty || this.v_hasValue(model);
      currentState.touched = !!currentState.touched;
      currentState.optional = this.v_isOptional(model);

      Vue.set(this.v_fieldsStates, model, currentState);
    },
    v_validate(model, rules) {
      const tests = mapObjectValues(rules,
        (parameters, rule) => this.v_test(model, rule, Object.assign(this.v_getDefaultRuleState(), parameters)));
      const failedTests = Object.values(tests).filter(test => test.active && !test.pass);

      const state = Object.assign(this.v_getDefaultFieldState(), this.v_fieldsStates[model]);

      state.valid = failedTests.length === 0; // all tests pass

      // any test failed and the model value exists or does not exist but is not optional
      state.invalid = failedTests.length > 0 && !(state.optional && !this.v_hasValue(model));

      return {
        state,
        tests,
      };
    },
    v_test(model, rule, settings) {
      if (!settings.active) return settings;

      return Object.assign({}, settings, {
        pass: getValidator(rule, model).execute(this.v_model[model], settings.parameter, {
          model,
          models: this.v_model,
          configuration: this.v_config,
          state: this.validation,
        }),
      });
    },
    v_handleTouch(modelName) {
      if (!this.v_fieldsStates[modelName] || this.v_fieldsStates[modelName].touched) return;
      this.v_updateFieldState(modelName, { touched: true });
      this.$emit('validation:field-state-update', modelName);
    },
    v_handleBlur(modelName) {
      const dirty = this.v_hasValue(modelName);
      if (!this.v_fieldsStates[modelName] || this.v_fieldsStates[modelName].dirty === dirty) return;
      this.v_updateFieldState(modelName, { dirty });
      this.$emit('validation:field-state-update', modelName);
    },
    v_isOptional(model) {
      const fieldConfig = this.v_getRules(model);

      return !fieldConfig.required
        || fieldConfig.required.active === false
        || fieldConfig.required.parameter === false;
    },
    v_fails(model) {
      const modelValidation = this.validation[model];
      if (!modelValidation) return true;
      return modelValidation.state.invalid
        || (!modelValidation.state.valid && !modelValidation.state.optional);
    },
    v_hasValue(modelName) {
      if (!this.v_model) return false; // needed for ie11 (?)

      return valueExists(this.v_model[modelName]);
    },
  },
});
