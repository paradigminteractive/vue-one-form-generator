export default Vue.extend({
  props: {
    validation: {
      type: Object,
      default: () => ({ tests: {} }),
    },
  },
  computed: {
    isValid() {
      return this.validation.state.dirty && this.validation.state.valid;
    },
    isInvalid() {
      return this.validation.state.dirty && this.validation.state.invalid;
    },
    validationClasses() {
      return {
        'is-valid': this.isValid,
        'is-invalid': this.isInvalid,
      };
    },
    validationError() {
      return this.v_Errors.length > 0 ? this.v_Errors[0] : '';
    },
    v_Errors() {
      return Object.values(this.validation.tests)
        .filter(test => test.active && !test.pass)
        .map(test => this.$i18n.t(test.error));
    },
  },
  methods: {
    getValidationDetails(rule) {
      return this.validation.tests[rule] || {};
    },
  },
});
