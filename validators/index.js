/* eslint-disable global-require */
import InjectedState from '../services/injected-state';

import street from './streetHouseNumber/street';
import streetMaxLength from './streetHouseNumber/streetMaxLength';
import houseNumber from './streetHouseNumber/houseNumber';
import houseNumberMaxLength from './streetHouseNumber/houseNumberMaxLength';

const validators = {
  minAge: require('./min-age'),
  maxAge: require('./max-age'),
  minLength: require('./min-length'),
  maxLength: require('./max-length'),
  pattern: require('./pattern'),
  required: require('./required'),
  type: require('./type'),
  streetHouseNumber: {
    street,
    streetMaxLength,
    houseNumber,
    houseNumberMaxLength,
  },
};

export default function validationRulesGetter(key, input, rule) {
  return validators[key](input, rule);
}

function findValidator(rule, model = '') {
  if (model && validators[model] && validators[model][rule]) {
    return {
      validator: validators[model][rule],
      name: `${model}-${rule}`,
    };
  }

  if (!validators[rule]) {
    throw new Error(`validator "${rule}" is not implemented`);
  }

  return {
    name: `${rule}`,
    validator: validators[rule],
  };
}

export const getValidator = (rule, model = '') => (
  {
    execute: (value, parameter, context) => {
      const { validator, name } = findValidator(rule, model);

      if (!validator) throw new Error(`${name} validator is not defined`);
      try {
        return validator(value, parameter, context);
      } catch (error) {
        if (InjectedState.isTestEnvironment()) {
          // eslint-disable-next-line max-len
          console.error(`%c${context.model}%c %c${name}${parameter !== undefined ? ` (${parameter})` : ''}%c validator crashed for value %c"${value}"`,
            'background: #444; color: #bada55; padding: 2px 4px; border-radius:2px',
            'background: auto; color: auto; padding: 0px;',
            'background: #444; color: #bada55; padding: 2px 4px; border-radius:2px',
            'background: auto; color: auto; padding: 0px;',
            'color: black; text-decoration: underline;');
          console.log(error);
        }
        return false;
      }
    },
  }
);
