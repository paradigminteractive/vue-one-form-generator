module.exports = (value, minLength) => value.length >= minLength;
