module.exports = (value, isRequired) => isRequired === false || !!value || value === 0;
