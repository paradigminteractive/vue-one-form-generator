import moment from 'moment';

module.exports = (value, maximumAge, { configuration: { birthDate } }) => {
  const age = moment().diff(moment(value, birthDate.date.parameter), 'years');

  return age <= maximumAge;
};
