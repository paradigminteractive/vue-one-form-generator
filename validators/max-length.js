module.exports = (value, maxLength) => (value.length || value.toString().length) <= maxLength;
