import XRegExp from 'xregexp';

module.exports = (value, pattern) => {
  const fullPattern = (pattern.startsWith('^') ? '' : '^')
    + pattern
    + (pattern.endsWith('$') ? '' : '$');

  return XRegExp(fullPattern).test(value);
};
