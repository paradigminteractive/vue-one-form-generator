import XRegExp from 'xregexp';

function formatter(value, { configuration: { streetHouseNumber: config } }) {
  const { houseNumber = '', streetName = '' } = XRegExp.exec(value, XRegExp(config.pattern.parameter)) || {};
  return { houseNumber, streetName };
}

export default validate => (value, parameter, context) => {
  const fields = formatter(value, context);
  return validate(fields, parameter, context);
};
