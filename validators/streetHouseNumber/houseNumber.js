import baseStreetHouseNumberValidator from './baseStreetHouseNumberValidator';

export default baseStreetHouseNumberValidator(
  ({ houseNumber }, pattern) => !!houseNumber
    && houseNumber !== '0'
    && new RegExp(pattern).test(houseNumber),
);
