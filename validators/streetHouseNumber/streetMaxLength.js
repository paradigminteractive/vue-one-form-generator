import baseStreetHouseNumberValidator from './baseStreetHouseNumberValidator';

export default baseStreetHouseNumberValidator(
  ({ streetName }, maxLength) => streetName.length <= maxLength,
);
