import baseStreetHouseNumberValidator from './baseStreetHouseNumberValidator';

export default baseStreetHouseNumberValidator(
  ({ streetName }, { charactersBlacklist }) => !!streetName && !charactersBlacklist.some(c => streetName.includes(c)),
);
