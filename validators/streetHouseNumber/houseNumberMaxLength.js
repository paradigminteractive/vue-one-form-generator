import baseStreetHouseNumberValidator from './baseStreetHouseNumberValidator';

export default baseStreetHouseNumberValidator(
  ({ houseNumber }, maxLength) => houseNumber.length <= maxLength,
);
