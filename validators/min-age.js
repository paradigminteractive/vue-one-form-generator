import moment from 'moment';

module.exports = (value, minimumAge, { configuration: { birthDate } }) => {
  const age = moment().diff(moment(value, birthDate.date.parameter), 'years');

  return age >= minimumAge;
};
