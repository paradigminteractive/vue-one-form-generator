/* eslint no-underscore-dangle: 0 */
import uniqId from 'uniqid';
import { pickBy, pick } from 'lodash';

import FormValidation from '../mixins/form-validation';

import CInput from './components/input';

import formSchemas from './schemas/forms';
import template from './form-generator.html';

module.exports = Vue.extend({
  name: 'CFormGenerator',
  components: {
    CInput,
  },
  mixins: [FormValidation({ modelProperty: 'fg_model', activeModelProperty: 'activeModel' })],
  props: {
    model: {
      type: Object,
      default: () => ({}),
    },
    schema: {
      type: [String, Object],
      required: true,
    },
    isLoading: {
      type: Boolean,
      default: false,
    },
    propsOverride: {
      /**
       * { fieldModel: { property: value }}
       */
      type: [Object],
      default: () => [],
    },
  },
  data() {
    return {
      fg_model: {},
      loading: false,
      identifier: uniqId('generated-form-'),
      fields: {},
    };
  },
  computed: {
    activeFields() {
      return pickBy(this.fields, (fieldComponent => !fieldComponent.hidden));
    },
    activeModel() {
      return pick(this.fg_model, Object.keys(this.activeFields));
    },
    fieldsProperties() {
      const fields = {};

      this.rows.forEach((row) => {
        row.forEach((field) => {
          const props = this.generateFieldProperties(field);
          Object.assign(fields, { [field.model]: props });
        });
      });

      return fields;
    },
    fieldSets() {
      return (this.formSchema.fieldSets || [])
        // flattening fieldSets, taking into account that schema can have slot instead of fields
        .map(fieldSet => (fieldSet.fields ? fieldSet.fields : fieldSet));
    },
    rows() {
      return this.fieldSets
        .filter(row => !row.slot);
    },
    formSchema() {
      if (typeof this.schema === 'object') {
        return this.schema;
      }
      return formSchemas[this.schema] || {};
    },
    footerSlotsCounts() {
      if (this._slotContents && this._slotContents.footer) {
        return this._slotContents.footer.childElementCount || 0;
      }

      return 0;
    },
  },
  watch: {
    model: {
      handler(newValues) {
        Object.keys(newValues)
          .filter(key => newValues[key] !== this.fg_model[key])
          .forEach(key => this.updateField({ model: key }, newValues[key]));
      },
      deep: true,
      immediate: true,
    },
    schema() {
      this.$nextTick(this.generateFields.bind(this));
      this.initialize();
    },
    fieldsProperties: {
      handler() {
        this.updateFields();
      },
      deep: true,
    },
    locale() {
      this.updateFields();
    },
  },
  ready() {
    this.generateFields();
    this.$nextTick(() => this.$emit('ready'));
  },
  created() {
    this.initialize();
    this.$on('validation:field-state-update', model => this.updateFieldComponent({ model }));
  },
  beforeDestroy() {
    this.$off('validation:field-state-update');
  },
  methods: {
    generateFields() {
      if (!this.rows) return;
      this.rows.forEach((fields) => {
        fields.forEach(this.regenerateFieldComponent.bind(this));
      });
    },
    updateFields() {
      if (!this.rows) return;
      this.rows.forEach((fields) => {
        fields.forEach(this.updateFieldComponent.bind(this));
      });
    },
    regenerateFieldComponent(field) {
      if (this.fieldComponentExists(field)) {
        this.updateFieldComponent(field);
      } else {
        this.spawnFieldComponent(field);
      }
    },
    updateFieldComponent(field) {
      const fieldComponent = this.getFieldComponent(field);
      if (!fieldComponent) return;

      Object.entries(this.getProperties(field))
        .forEach(([property, value]) => {
          Vue.set(fieldComponent, property, value);
        });
      fieldComponent.refresh();
    },
    spawnFieldComponent(field) {
      const FieldComponent = this.$options.components[this.getFormattedComponentName(field.component)]
        || Vue.component(field.component);

      const props = this.getProperties(field);

      const el = this.getFieldPlaceholder(field);

      if (!el) return;

      const form = this;
      const fieldComponent = new FieldComponent({
        el,
        propsData: props,
        props: {
          hidden: {
            type: Boolean,
            default: false,
          },
        },
        parent: this,
        replace: true,
        watch: {
          hidden() {
            this.refresh();
          },
        },
        ready() {
          this.refresh();
        },
        events: {
          input(value) {
            return form.updateField(field, value);
          },
        },
        methods: {
          isInDOM() {
            return document.body.contains(this.$el);
          },
          isDestroyed() {
            return !this.$el;
          },
          /**
           * this is a programmatic substitute of v-if
           */
          refresh() {
            // eslint-disable-next-line no-unused-expressions
            if (this.hidden) this.isInDOM() && this.$remove();
            else {
              this.$nextTick(() => {
                if (this.hidden || this.isInDOM() || this.isDestroyed()) return;

                const fieldWrapper = form.getFieldWrapperElement(field);
                if (fieldWrapper) this.$appendTo(fieldWrapper);
              });
            }
          },
        },
      });

      Vue.set(this.fields, field.model, fieldComponent);
    },
    generateFieldProperties(field) {
      return Object.assign({}, field.props, {
        form: this.formSchema.formId,
        label: this.$i18n.t(field.props.label),
        placeholder: this.$i18n.t(field.props.placeholder),
        class: this.getFieldClasses(field),
      }, this.getDynamicProperties(field));
    },
    getDynamicProperties(field) {
      return this.propsOverride[field.model] || {};
    },
    getProperties(field) {
      return Object.assign({
        value: this.fg_model[field.model],
        validation: this.validateModel(field.model),
      }, this.fieldsProperties[field.model]);
    },
    getFormattedComponentName(dashedName) {
      return dashedName.charAt(0).toUpperCase() + dashedName.slice(1).replace(/-([a-z])/g, str => str[1].toUpperCase());
    },
    getFieldPlaceholder(field) {
      return this.$els.form
        && this.$els.form.querySelector(`[data-model-placeholder="${this.generateFieldIdentifier(field)}"]`);
    },
    getFieldWrapperElement(field) {
      return this.$els.form
        && this.$els.form.querySelector(`[data-model-wrapper="${this.generateFieldIdentifier(field)}"]`);
    },
    generateFieldIdentifier(field) {
      return `${this.identifier}-${field.model}`;
    },
    initialize() {
      this.resetFieldComponents();

      this.rows.forEach((fieldSet) => {
        fieldSet.forEach((field) => {
          if (!this.fg_model[field.model] && field.props.value) {
            this.fg_model[field.model] = field.props.value;
          } else if (!this.fg_model[field.model] && !field.props.value) {
            this.fg_model[field.model] = '';
          }
        });
      });

      this.resetValidation();
      this.$emit('update', this.fg_model);
    },
    resetFieldComponents() {
      Object.values(this.fields).forEach(field => field.$destroy());
      this.fields = {};
    },
    validateAndSubmit() {
      this.loading = true;
      this.$emit('submit', this.fg_model);
      this.validate();
      this.updateFields();

      if (this.isValid) {
        this.$emit('valid-submit', this.fg_model);
        return true;
      }

      this.$emit('invalid-submit', this.fg_model);
      this.loading = false;
      return false;
    },
    updateField(field, value) {
      Vue.set(this.fg_model, field.model, value);
      Vue.set(this.model, field.model, value);
      this.updateFieldComponent(field);
      this.$emit('update', this.model);
    },
    fieldComponentExists(field) {
      return !!this.getFieldComponent(field);
    },
    getFieldComponent(field) {
      return this.fields[field.model];
    },
    onSuccessfullSubmit() {
      this.loading = false;
    },
    getFieldWrapperClasses(field) {
      return [`c-form-generator__field-wrapper-${field.model}`];
    },
    getFieldClasses(field) {
      return [...(field.classes || []), `c-form-generator__field-${field.model}`];
    },
  },
  template,
});
