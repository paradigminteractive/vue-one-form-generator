## Form generator for Vue 1

### Features:
 - generates form based on schema (provided as a json)
- json api to define complex form fields
- form can have custom slots
- all fields properties are dynamic (can be defined as a computed property of a component hosting the form)
- model based validation:
    - highly configurable (simple json api)
    - dynamic (can be modified on the fly)
    - supports localization out of the box
    - seamless integration of custom validators
    - supports ascynchronous validators
    - can be easily adjusted to work with any custom input components (by form-field-validation mixin)
    - simple debugging with Vue DevTools

