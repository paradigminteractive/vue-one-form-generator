import template from './hint.html';

module.exports = Vue.extend({
  name: 'CHint',
  props: {
    text: {
      type: String,
      default: '',
    },
  },
  template,
});
