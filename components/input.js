import template from './input.html';
import FormFieldValidation from '../mixins/form-field-validation';
import FormElement from './form-element';

import CHint from './hint';
import CError from './error';
import CSuggestion from './suggestion';
import CLabel from './label';

const INPUT_TYPES = ['text', 'number', 'email', 'password', 'tel', 'date'];

const INPUT_AUTOCOMPLETE_TYPES = [
  'on',
  'off',
  'given-name',
  'family-name',
  'email',
  'new-password',
  'current-password',
  'street-address',
  'address-level2',
  'postal-code',
  'bday',
  'tel',
];

module.exports = FormElement.extend({
  name: 'CInput',
  components: {
    CHint,
    CError,
    CSuggestion,
    CLabel,
  },
  mixins: [FormFieldValidation],
  props: {
    value: {
      type: [String, Number],
      default: '',
    },
    type: {
      type: String,
      default: 'text',
      validator: value => INPUT_TYPES.includes(value),
    },
    placeholder: {
      type: String,
      default: null,
    },
    minlength: {
      type: Number,
      default: null,
    },
    maxlength: {
      type: Number,
      default: null,
    },
    pattern: {
      type: String,
      default: null,
    },
    autocomplete: {
      type: String,
      default: 'on',
      validator: value => INPUT_AUTOCOMPLETE_TYPES.includes(value),
    },
    displayLabel: {
      type: String,
      default: 'on-input',
    },
    visibilityToggle: {
      type: Boolean,
      default: false,
    },
    errorPosition: {
      type: String,
      default: 'below-input',
    },
    hint: {
      type: String,
      default: '',
    },
    info: {
      type: String,
      default: '',
    },
    infoTitle: {
      type: String,
      default: '',
    },
    suggestion: {
      type: String,
      default: '',
    },
    maskKey: {
      type: String,
      default: '',
    },
  },
  data() {
    return {
      valueVisibile: false,
      focused: false,
    };
  },
  watch: {
    fieldType() {
      this.$nextTick(() => {
        if (!this.focused) return;
        this.$els.input.selectionStart = this.value.length;
        this.$els.input.selectionEnd = this.value.length;
      });
    },
  },
  computed: {
    classes() {
      return {
        'has-info': !!this.info,
        'has-label': this.showLabel,
        'is-disabled': this.disabled,
        'is-focused': this.focused,
        'has-mask': this.maskKey,
        ...this.validationClasses,
      };
    },
    isLabelVisibleAlways() {
      return (Boolean(this.value) && this.displayLabel === 'on-input') || this.displayLabel === 'always';
    },
    showLabel() {
      return this.label && this.errorPosition !== 'replace-label' && this.isLabelVisibleAlways;
    },
    showError() {
      return !this.focused;
    },
    errorReplaceLabel() {
      return this.showError && this.validationError && this.errorPosition === 'replace-label';
    },
    errorBelowInput() {
      return this.showError && this.validationError && this.errorPosition === 'below-input';
    },
    showSuggestion() {
      return this.suggestion && this.suggestion !== this.value;
    },
    mask() {
      return this.$i18n.t(this.maskKey);
    },
    fieldType() {
      if (this.visibilityToggle) {
        return this.valueVisibile ? 'text' : 'password';
      }
      return this.type;
    },
    textToggle() {
      if (this.valueVisibile) {
        return this.$i18n.t('formFields.passwordVisibility.hide');
      }
      return this.$i18n.t('formFields.passwordVisibility.show');
    },
    maxLength() {
      return this.getValidationDetails('maxLength').parameter || '';
    },
    hasEmptyValue() {
      return !this.value;
    },
    showVisibilityToggle() {
      return this.visibilityToggle && !this.hasEmptyValue;
    },
  },
  methods: {
    onFocus() {
      this.focused = true;
      this.validation.handlers.touched();
    },
    onBlur(event) {
      this.focused = false;
      this.validation.handlers.blurred();
      this.$emit('blur', event, event.target.value);
    },
    onKeyDown(event) {
      this.validation.handlers.touched();
      this.$emit('keydown', event, event.target.value);
    },
    onInput(event) {
      this.validation.handlers.touched();
      this.$emit('input', event.currentTarget.value, event);
    },
    toggleVisibility() {
      this.valueVisibile = !this.valueVisibile;
    },
    resetValue() {
      this.$emit('input', '');
    },
  },
  template,
});
