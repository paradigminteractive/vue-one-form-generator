module.exports = Vue.extend({
  name: 'FormElement',
  props: {
    autofocus: Boolean,
    disabled: Boolean,
    form: {
      type: String,
      default: null,
      required: true,
    },
    label: {
      type: String,
      default: '',
    },
    name: {
      type: String,
      default: '',
      required: true,
    },
    required: {
      type: Boolean,
      default: null,
    },
  },
  computed: {
    id() {
      return `${this.form}-${this.name}`;
    },
    simpleId() {
      return this.name;
    },
  },
});
