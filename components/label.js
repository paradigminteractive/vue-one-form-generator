import template from './label.html';

module.exports = Vue.extend({
  name: 'CLabel',
  props: {
    text: {
      type: String,
      default: '',
    },
    inputId: {
      type: String,
      required: true,
    },
  },
  template,
});
