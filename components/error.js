import template from './error.html';

module.exports = Vue.extend({
  name: 'CError',
  props: {
    errorMessage: {
      type: String,
      default: '',
    },
    inputId: {
      type: String,
      required: true,
    },
  },
  template,
});
