import template from './suggestion.html';

module.exports = Vue.extend({
  name: 'CSuggestion',
  props: {
    text: {
      type: String,
      default: '',
    },
  },
  template,
});
