export default function valueExists(value) {
  return !!value || value === 0;
}
