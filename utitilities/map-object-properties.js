export default (object, mapper) => Object.entries(object)
  .map(([property, value]) => ({ [property]: mapper(value, property) }))
  .reduce(((r, c) => Object.assign(r, c)), {});
