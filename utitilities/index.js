import valueExists from './value-exists';
import generateObject from './generate-object';
import mapObjectValues from './map-object-properties';

export default {
  valueExists,
  mapObjectValues,
  generateObject,
};

export {
  valueExists,
  mapObjectValues,
  generateObject,
};
