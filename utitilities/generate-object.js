export default (keys, propertyGenerator) => keys
  .map(key => ({ [key]: propertyGenerator(key) }))
  .reduce(((r, c) => Object.assign(r, c)), {});
